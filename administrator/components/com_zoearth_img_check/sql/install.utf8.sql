
-- 20160811 zoearth 紀錄對應檔案位置
CREATE TABLE IF NOT EXISTS `#__zoearth_img_flow_files` (
  `fileName` varchar(255) NOT NULL COMMENT '檔案名稱',
  `fileTime` datetime NOT NULL COMMENT '檔案時間',
  `fileServer` int(1) NOT NULL DEFAULT '0' COMMENT '檔案SERVER',
  `fileUrl` varchar(255) NOT NULL DEFAULT '' COMMENT '檔案位置',
  `driveUrl` varchar(255) NOT NULL DEFAULT '' COMMENT 'drive位置',
  PRIMARY KEY (`fileName`,`fileTime`,`fileServer`),
  KEY `fileUrl` (`fileUrl`),
  KEY `driveUrl` (`driveUrl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Zoearth檔案FTP同步資料';

-- 20160820 zoearth 紀錄Server 紀錄
CREATE TABLE IF NOT EXISTS `#__zoearth_img_flow_server` (
  `HostName` varchar(255) NOT NULL COMMENT '伺服器名稱',
  `getTime` datetime NOT NULL COMMENT '檢查時間',
  `state` varchar(10) NOT NULL DEFAULT '' COMMENT '狀態碼',
  `note` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  PRIMARY KEY (`HostName`,`getTime`),
  KEY `state` (`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Zoearth檔案FTP檢查資料';