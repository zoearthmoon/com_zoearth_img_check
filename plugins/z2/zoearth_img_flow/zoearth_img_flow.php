<?php
defined('_JEXEC') or die ('Restricted access');

class plgZ2Zoearth_Img_Flow extends JPlugin 
{
    //20160812 zoearth 上傳檔案與更新檔案
    function onCallCrontab()
    {
        require_once JPATH_SITE.DS.'plugins'.DS.'z2'.DS.'zoearth_img_flow'.DS.'zoearth_img_flow_helper.php';
        
		if ($this->params->get('uploadType') == 'G' )
		{
			return $this->onCallCrontabGoogleDrive();
		}
		else
		{
			return $this->onCallCrontabFtp();
		}
    }
	
	function onCallCrontabGoogleDrive()
	{
		return ZoearthImgFlowHelper::GDuploadToDrive($error);
	}
	
	function onCallCrontabFtp()
	{
        //20170202 zoearth 取得設定
        $ftpSets = ZoearthImgFlowHelper::getFTPsets();
        $tError  = '';
        
        foreach ($ftpSets as $ftpSet)
        {
            ZoearthImgFlowHelper::$ftp_url      = $ftpSet['ftp'];
            ZoearthImgFlowHelper::$ftp_dir      = '/'.trim($ftpSet['dir']);
            ZoearthImgFlowHelper::$ftp_account  = $ftpSet['account'];
            ZoearthImgFlowHelper::$ftp_password = $ftpSet['password'];
            ZoearthImgFlowHelper::$ftp_web_url  = $ftpSet['url'];
            ZoearthImgFlowHelper::$ftp_servers  = $ftpSet['servers'];
            //檢查SERVER
            if (!ZoearthImgFlowHelper::checkServer($error))
            {
                $tError .= "\r\n".$error;
                continue;
            }
            
            //建立FTP
            if (!ZoearthImgFlowHelper::connectToFtp($this,$error))
            {
                $tError .= "\r\n".$error;
                continue;
            }
            
            //上傳FTP
            if (!ZoearthImgFlowHelper::uploadToFtp($error) && $error != '')
            {
                $tError .= "\r\n".$error;
                continue;
            }
            
            //20160812 zoearth 同步步驟
            //刪除同檔案不同時間保留最新的檔案資料
            if (!ZoearthImgFlowHelper::deleteOldFile($error))
            {
                $tError .= "\r\n".$error;
                continue;
            }
            
            //同步檔案
            //if (!ZoearthImgFlowHelper::SyncFtp($error))
            //{
            //    return $error;
            //}

            ZoearthImgFlowHelper::ftp_close();
        }
        return $tError;
	}
}