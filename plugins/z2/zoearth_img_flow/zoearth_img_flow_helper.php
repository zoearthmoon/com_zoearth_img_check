<?php
defined('_JEXEC') or die ('Restricted access');

//20160811 zoearth 這邊是主要功能
class ZoearthImgFlowHelper 
{
    static $dir          = 'flow';
    static $conn         = FALSE;
    static $ftp_sets     = FALSE;
    static $ftp_url      = FALSE;
    static $ftp_dir      = FALSE;
    static $ftp_account  = FALSE;
    static $ftp_password = FALSE;
    static $ftp_web_url  = FALSE;
    static $ftp_servers  = FALSE;
    //替換資料用
    static $ftp_web_urls = array();
    static $preFile      = 50;
    
    //取得設定
    public static function getFTPsets()
    {
        if (is_array(self::$ftp_sets))
        {
            return self::$ftp_sets;
        }

        $plugin        = JPluginHelper::getPlugin('z2','zoearth_img_flow');
        $params        = json_decode($plugin->params,TRUE);
        $ftp_web_datas = isset($params['ftp_web_datas']) ? $params['ftp_web_datas']:'';
        $ftpSets       = array();
        if (preg_match_all('/(.*)>>>(.*):(.*)@(.*)\/(.*)/',$ftp_web_datas,$matches))
        {
            foreach ($matches[1] as $k=>$v)
            {
                $ftpSets[$k] = array(
                        //網址
                        'url'      => $matches[1][$k],
                        //帳號
                        'account'  => $matches[2][$k],
                        //密碼
                        'password' => $matches[3][$k],
                        //FTP
                        'ftp'      => $matches[4][$k],
                        //資料夾
                        'dir'      => $matches[5][$k],
                        //目標伺服器
                        'servers'  => array(),
                    );
            }
            
            //這邊應該依照伺服器數量決定要取得那些伺服器編號檔案
            for ($i=0;$i<=9;$i)
            {
                foreach ($ftpSets as $k=>$as)
                {
                    if ($i > 9)break;
                    $ftpSets[$k]['servers'][] = $i;
                    $ftp_web_urls[$i]         = $ftpSets[$k]['url'];
                    $i++;
                }
            }
        }
        self::$ftp_sets     = $ftpSets;
        self::$ftp_web_urls = $ftp_web_urls;
        return self::$ftp_sets;
    }
    
    //檢查SERVER
    public static function checkServer(&$error='')
    {
        $ftp_web_url = self::$ftp_web_url;
        
        $ch = curl_init($ftp_web_url);
        curl_setopt($ch,CURLOPT_TIMEOUT,3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //不直接顯示結果
        curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $db = Z2HelperQueryData::getDB();
        $data = array(
                'HostName' => $ftp_web_url,//伺服器名稱
                'getTime'  => date('Y-m-d H:i:s'),//檢查時間
                'state'    => (int)$http_code,//狀態碼
                'note'     => '',//描述
        );
        $data = (object)$data;
        $db->insertObject('#__zoearth_img_flow_server',$data);
        
        //20160821 zoearth 刪除舊的
        $query = $db->getQuery(true);
        $query->delete('#__zoearth_img_flow_server')
            ->where('getTime < '.$db->quote(date('Y-m-d H:i:s',strtotime("-6 month"))));
        $db->setQuery($query);
        $db->execute();
        $error = $http_code;
        return $http_code == '200' ? TRUE:FALSE;
    }
    
    //建立FTP連線
    public static function connectToFtp(PlgZ2Zoearth_Img_Flow $Jplg,&$error='')
    {
        //開啟FTP
        $ftp_url      = self::$ftp_url;
        $ftp_dir      = self::$ftp_dir;
        $ftp_account  = self::$ftp_account;
        $ftp_password = self::$ftp_password;
        
        self::$ftp_url = $ftp_url;
        self::$ftp_dir = $ftp_dir;
        
        if (!($ftp_url && $ftp_account && $ftp_password ))
        {
            $error = 'ERROR 0041 Zoearth_Img_Flow 缺少FTP設定';
            return FALSE;
        }
        
        self::$conn = ftp_connect($ftp_url, 21, 20);
        if (!self::$conn)
        {
            $error = 'ERROR 0056 Zoearth_Img_Flow 無法連線FTP@'.$ftp_url;
            return FALSE;
        }
        ftp_set_option(self::$conn, FTP_TIMEOUT_SEC, 5);
        $fLogin = @ftp_login(self::$conn,$ftp_account,$ftp_password);
        if (!$fLogin)
        {
            $error = 'ERROR 0128 Zoearth_Img_Flow 無法登入FTP@'.$ftp_url;
            return FALSE;
        }
        //被動模式
        ftp_pasv(self::$conn, true);
        
        return TRUE;
    }
    
    //取得FTP上面檔案
    public static function SyncFtp(&$error='')
    {
        //取得目前FTP對應資料夾上所有檔案
        if (!self::$conn)
        {
            $error = 'ERROR 0016 Zoearth_Img_Flow 沒有FTP連線';
            return FALSE;
        }
        $ftpDirs = ftp_nlist(self::$conn,self::$ftp_dir.'/'.self::$dir);
        
        //取得目前資料庫中有對應的所有檔案
        $db = Z2HelperQueryData::getDB();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__zoearth_img_flow_files')
            ->where('fileUrl != "" ');
        $db->setQuery($query,0,9999);
        $rows = $db->loadObjectList();
        if (!$rows)
        {
            $rows = array();
        }
        
        $webFiles = array();
        foreach ($rows as $row)
        {
            $webFiles[$row->fileUrl] = $row->fileUrl;
        }
        
        //比對檔案
        foreach ($ftpDirs as $fileDir)
        {
            //改成子資料夾
            if ($fileDir == '..' || $fileDir == '.' )
            {
                continue;
            }
            
            $ftpFiles = ftp_nlist(self::$conn,self::$ftp_dir.'/'.self::$dir.'/'.$fileDir);
            if (!(is_array($ftpFiles) && count($ftpFiles) > 0 ))
            {
                continue;
            }
            
            foreach ($ftpFiles as $file)
            {
                if ($file == '..' || $file == '.' )
                {
                    continue;
                }
                
                //刪除FTP沒有 資料庫有的資料
                //刪除資料庫沒有 FTP有的資料
                $ftpFile = '/'.self::$dir.'/'.$fileDir.'/'.$file;
                
                //有對應到
                if (in_array($ftpFile, $webFiles))
                {
                    unset($webFiles[$ftpFile]);
                }
                else
                {
                    //刪除檔案
                    if (!ftp_delete(self::$conn,self::$ftp_dir.'/'.self::$dir.'/'.$fileDir.'/'.$file))
                    {
                        $error = 'ERROR 0091 Zoearth_Img_Flow 刪除檔案失敗 :'.self::$ftp_dir.'/'.self::$dir.'/'.$fileDir.'/'.$file;
                        return FALSE;
                    }
                }
            }
        }
        
        //需要刪除沒有對應檔案的資料
        if (!(is_array($webFiles) && count($webFiles) > 0 ))
        {
            return TRUE;
        }
        
        foreach ($webFiles as $webFile)
        {
            //刪除資料
            $query = $db->getQuery(true);
            $query->delete('#__zoearth_img_flow_files')
                ->where('fileUrl = '.$db->quote($webFile));
            $db->setQuery($query);
            $db->execute();
        }
        return TRUE;
    }
    
    //刪除同檔案不同時間保留最新的檔案資料
    public static function deleteOldFile(&$error='')
    {
        $sql = 'SELECT `fileName` , cc
        FROM (
            SELECT COUNT( 1 ) AS cc, `fileName`
            FROM `#__zoearth_img_flow_files`
            WHERE 1
            GROUP BY `fileName`
        ) gc
        WHERE cc >=2';
        
        $db = Z2HelperQueryData::getDB();
        $db->setQuery($sql,0,999);
        $rows = $db->loadObjectList();
        
        if (!$rows)
        {
            return TRUE;
        }
        
        foreach ($rows as $row)
        {
            //取得舊檔案
            $query = $db->getQuery(true);
            $query->select('*')
                ->from('#__zoearth_img_flow_files')
                ->where('fileName = '.$db->quote($row->fileName).' ')
                ->order('fileTime ASC');
            $db->setQuery($query);
            $delRow = $db->loadObject();
            if (!$delRow)
            {
                continue;
            }
            
            //刪除舊檔案
            $query = $db->getQuery(true);
            $query->delete('#__zoearth_img_flow_files')
                ->where('fileName = '.$db->quote($delRow->fileName))
                ->where('fileTime = '.$db->quote($delRow->fileTime));
            $db->setQuery($query);
            $db->execute();
        }
        return TRUE;
    }
    
    //取得檔案名稱
    public static function getFileFtpName($fileName,$fileTime)
    {
        if (!preg_match('/\.([^.]*)$/',$fileName,$match))
        {
            return FALSE;
        }
        
        return 'F_'.md5($fileName.$fileTime).'.'.strtolower($match[1]);
    }
    
    public static function GD_mime_content_type($filename)
    {
        switch (strtolower(substr($filename,-3,3)))
        {
            case 'gif': return 'image/gif';break;
            case 'bmp': return 'image/bmp';break;
            case 'png': return 'image/png';break;
            default:
            case 'peg': 
            case 'jpg': return 'image/jpeg';break;
        }
        return 'image/jpeg';
    }
    
	public static function GDreadVideoChunk ($handle, $chunkSize)
	{
		$byteCount = 0;
		$giantChunk = "";
		while (!feof($handle)) {
			// fread will never return more than 8192 bytes if the stream is read buffered and it does not represent a plain file
			$chunk = fread($handle, 8192);
			$byteCount += strlen($chunk);
			$giantChunk .= $chunk;
			if ($byteCount >= $chunkSize)
			{
				return $giantChunk;
			}
		}
		return $giantChunk;
	}
	
    //上傳uploadToDrive
    public static function GDuploadToDrive(&$error='')
    {
        //取得目前沒有對應檔案的資料
        $db = Z2HelperQueryData::getDB();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__zoearth_img_flow_files')
            ->where('driveUrl = "" ');
        //取得一次執行數量
        $preFile    = 9999;
		$countFileC = (int)(self::$preFile/2.5);
        $db->setQuery($query,0,$preFile);
        $rows = $db->loadObjectList();
        if (!$rows)
        {
            return TRUE;
        }
        
		//開啟資料連線
        //是否有token
        $query = $db->getQuery(true);
        $query->select('t.*')
            ->from('#__z2_ext_google_api_simple_token AS t')
            ->where('t.auth = "drive" ')
            ->order('t.tokenIdate DESC');
        $db->setQuery($query,0,1);
        $row = $db->loadObject();
        if (!$row)
        {
            $error = 'ERROR 9825 NO DRIVE TOKEN';return FALSE;
        }
        
        $token  = json_decode($row->token,TRUE);
        $params = json_decode($row->params,TRUE);
        
        //是否有class
        $classFile = JPATH_ROOT.DS.'plugins/z2/z2_ext_google_api_simple/google-api-php-client-1.1.8/src/Google/autoload.php';
        if (!file_exists($classFile))
        {
            $error = 'ERROR 9355 NO CLASS FILE';return FALSE;
        }
        require_once $classFile;

        $plugin    = JPluginHelper::getPlugin('z2','zoearth_img_flow');
        $paramSets = json_decode($plugin->params,TRUE);
		
		//$paramSets['google_service_account_name'] //目標google帳號
		//$paramSets['google_open_dir_id'] //目標drive資料夾ID
		
		if (!(isset($paramSets['google_service_account_name']) && $paramSets['google_service_account_name'] != '' && isset($paramSets['google_open_dir_id']) && $paramSets['google_open_dir_id'] != '' ))
		{
			$error = 'ERROR 3556 NO SET DRIVE';return FALSE;
		}
		
        $client = new Google_Client();
        $client->setClientId($params['client_id']);
        $client->setClientSecret($params['client_secret']);
        $client->addScope("https://www.googleapis.com/auth/drive");
        $client->setAccessToken($row->token);
        
        $service_account_name = $paramSets['google_service_account_name'];
        //notasecret
        $key  = file_get_contents( __DIR__.'/drive_upload_IMG-b8d76ed55acd.p12');
        $cred = new Google_Auth_AssertionCredentials(
                $service_account_name,
                array('https://www.googleapis.com/auth/drive'),
                $key
        );
        
        $client->setAssertionCredentials($cred);
        if ($client->getAuth()->isAccessTokenExpired()) {
                $client->refreshToken($params['refresh_token']);
                $token = $client->getAccessToken();
                $client->setAccessToken($token);
        }
        
        $service = new Google_Service_Drive($client);
        
        //準備上傳
        if (!$client->getAccessToken())
        {
            $error = 'ERROR 5454 TOKEN';return FALSE;
        }
        
        $file = new Google_Service_Drive_DriveFile();
		$cc = 0;
        foreach ($rows as $row)
        {
            $fileName = $row->fileName;
            $fileTime = $row->fileTime;
            //檔案是否存在
            if (!file_exists(JPATH_ROOT.$fileName))
            {
				//刪除沒有檔案的
				$query = $db->getQuery(true);
				$query->delete('#__zoearth_img_flow_files')
					->where('fileName = '.$db->quote($row->fileName));
				$db->setQuery($query);
				$db->execute();
                continue;
            }
			$cc++;
			if ($cc > $countFileC)
			{
				break;
			}
            //取得檔案名稱
            $fileType = self::GD_mime_content_type(JPATH_ROOT.$fileName);
            $gfile    = JPATH_ROOT.$fileName;
            
            $file->title = basename($gfile);
            $file->setName(basename($gfile));
            $file->setParents(array($paramSets['google_open_dir_id']));
            $chunkSizeBytes = 1 * 1024 * 1024;
			//$chunkSizeBytes = 8192;
            //$client->setDefer(true);
			//echo $client->getClassConfig("Google_Http_Request", "enable_gzip_for_uploads").'---test95626';
			//$client->setClassConfig("Google_Http_Request", "enable_gzip_for_uploads",true);
			try {
				$result = $service->files->create(
					$file,
					array(
					'data' => file_get_contents($gfile),
					'mimeType' => $fileType,
					'uploadType' => 'media'
					)
				);
			} catch (Exception $e) {
				continue;
			}

			
			/*
            $request = $service->files->create($file);
			//echo $fileType.'---tttt';exit();
            $media   = new Google_Http_MediaFileUpload(
                    $client,
                    $request,
                    $fileType,
                    null,
                    false,
                    $chunkSizeBytes,
					true
                );
				
            $media->setFileSize(filesize($gfile));
            $status = false;
            $handle = fopen($gfile, "rb");
			$tt = 0;
			
            while (!$status && !feof($handle)) {
				
                $chunk  = self::GDreadVideoChunk($handle, $chunkSizeBytes);
				//echo $status.'---ggg---'.$fileType.'---test102';exit();
                $status = $media->nextChunk($chunk);
				
				$tt++;
				if ($tt > 99)
				{
					echo 'TTTTTTTTTTTTTTT';exit();
				}
            }
            $result = false;
            if ($status != false)
            {
                $result = $status;
            }
            fclose($handle);
			*/
            //更新資料庫
            $data = array(
                    'fileName' => $fileName,//檔案名稱
                    'driveUrl' => $result->id,//檔案位置
                    );
            $data = (object)$data;
            $db->updateObject('#__zoearth_img_flow_files',$data,'fileName');
        }
        return TRUE;
    }
	
    //上傳FTP
    public static function uploadToFtp(&$error='')
    {
        //取得目前沒有對應檔案的資料
        $db = Z2HelperQueryData::getDB();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__zoearth_img_flow_files')
            ->where('fileUrl = "" ');
        //取得一次執行數量
        $preFile = max(count(self::$ftp_sets),1);
        $preFile = ceil(self::$preFile / $preFile);
        
        //這邊應該依照伺服器數量決定要取得那些伺服器編號檔案
        if (!(is_array(self::$ftp_servers) && count(self::$ftp_servers) > 0 ))
        {
            $error = 'ERROR 0292 :目標伺服器錯誤';
            return FALSE;
        }
        $query->where('fileServer IN ('.implode(',',self::$ftp_servers).') ');
        
        $db->setQuery($query,0,$preFile);
        $rows = $db->loadObjectList();
        if (!$rows)
        {
            return TRUE;
        }
        
        //print_r(ftp_nlist($conn,'/'));
        
        //更改資料夾
        if (!ftp_chdir(self::$conn,self::$ftp_dir.'/'.self::$dir))
        {
            $error = 'ERROR 0065 Zoearth_Img_Flow 無法進入指定資料夾:'.self::$ftp_dir.'/'.self::$dir;
            ftp_close(self::$conn);
            return FALSE;
        }

        foreach ($rows as $row)
        {
            $fileName = $row->fileName;
            $fileTime = $row->fileTime;
            //檔案是否存在
            if (!file_exists(JPATH_ROOT.$fileName))
            {
                continue;
            }
    
            //取得檔案名稱
            $upName = self::getFileFtpName($fileName, $fileTime);
            if (!$upName)
            {
                $error = 'ERROR 0070 Zoearth_Img_Flow 無法取得檔案名稱:'.$fileName;
                ftp_close(self::$conn);
                return FALSE;
            }
            
            //指定資料夾
            $dirName = substr($upName,2,2);
            if (!ftp_chdir(self::$conn,self::$ftp_dir.'/'.self::$dir.'/'.$dirName))
            {
                if (!ftp_mkdir(self::$conn,self::$ftp_dir.'/'.self::$dir.'/'.$dirName))
                {
                    $error = 'ERROR 0255 Zoearth_Img_Flow 無法新增子資料夾:'.self::$ftp_dir.'/'.self::$dir.'/'.$dirName;
                    ftp_close(self::$conn);
                    return FALSE;
                }
                
                if (!ftp_chdir(self::$conn,self::$ftp_dir.'/'.self::$dir.'/'.$dirName))
                {
                    $error = 'ERROR 0261 Zoearth_Img_Flow 無法進入指定子資料夾:'.self::$ftp_dir.'/'.self::$dir.'/'.$dirName;
                    ftp_close(self::$conn);
                    return FALSE;
                }
            }
            
            //上傳資料
            if (ftp_put(self::$conn,$upName,JPATH_ROOT.$fileName,FTP_BINARY))
            {
                //更新資料庫
                $data = array(
                        'fileName' => $fileName,//檔案名稱
                        'fileUrl'  => '/'.self::$dir.'/'.$dirName.'/'.$upName,//檔案位置
                        );
                $data = (object)$data;
                $db->updateObject('#__zoearth_img_flow_files',$data,'fileName');
            }
            else
            {
                $error = 'ERROR 0080 Zoearth_Img_Flow 無法上傳檔案:'.$fileName;
                ftp_close(self::$conn);
                return FALSE;
            }
        }
        //ftp_close($conn);
        return TRUE;
    }
    
    //共同取得檔案名稱
    public static function getFileName($file)
    {
		$file = str_replace('https://'.$_SERVER['HTTP_HOST'].'','',$file);
        $file = str_replace('http://'.$_SERVER['HTTP_HOST'].'','',$file);
        $file = str_replace('//','/',$file);
        $file = substr($file,0,1) != '/' ? '/'.$file:$file;
        return $file;
    }
    
    //取得伺服器最新檢查狀態
    public static function isServerActive($ftp_web_url)
    {
        $db = Z2HelperQueryData::getDB();
        $query = $db->getQuery(true);
        $query->select('state')
            ->from('#__zoearth_img_flow_server')
            ->where('HostName = '.$db->quote($ftp_web_url).' ')
            ->order('getTime DESC');
        $db->setQuery($query,0,1);
        $row = $db->loadObject();
        if ($row && $row->state != '200' )
        {
            return FALSE;
        }
        return TRUE;
    }
    
    //替換資料
    public static function render($html,&$setBody=FALSE)
    {
        $plugin    = JPluginHelper::getPlugin('z2','zoearth_img_flow');
        $paramSets = json_decode($plugin->params,TRUE);
        if ($paramSets['uploadType'] != 'G' )
        {
            //$ftp_web_url  = self::$ftp_web_url;
            //取得網址列表
            $ftp_sets     = self::getFTPsets();
            $ftp_web_urls = self::$ftp_web_urls;

            foreach ($ftp_sets as $ftp_set)
            {
                $ftp_web_url = $ftp_set['url'];
                //20160820 zoearth 檢查最後一次檢查必須是成功
                if ((!self::isServerActive($ftp_web_url) || !$ftp_web_url) && is_array($ftp_set['servers']) && count($ftp_set['servers']) > 0 )
                {
                    //取消該伺服器轉換
                    foreach ($ftp_set['servers'] as $sK)
                    {
                        unset($ftp_web_urls[$sK]);
                    }
                }
            }
        }

        $files = array();
        $db = Z2HelperQueryData::getDB();
        //取得所有圖片與JS與CSS
        //取消JS轉換(因為如果JS網站失效，整個網站都出不來了)
        $html = str_replace('content="http','content____http',$html);
        if(!preg_match_all('/\"([^" ;]{1,}\.(jpg|png|gif))/i', $html,$match))
        {
            return FALSE;
        }
        
        if (!(is_array($match[1]) && count($match[1]) > 0 ))
        {
            return FALSE;
        }
        
        $sqlS     = '';
        $fReplace = array();
        foreach ($match[1] as $orgFile)
        {
            //整理絕對路徑
            $file = self::getFileName($orgFile);
            
            //檔案是否存在
            if (!file_exists(JPATH_ROOT.$file))
            {
                continue;
            }
            
            //檔案時間(只有/cache/皆為相同時間)
            $fileTime = date('Y-m-d H:i:s',filemtime(JPATH_ROOT.$file));
            //如果是 JS與CSS 則抓檔案時間
            //如果是JS與CSS則需要紀錄時間，其他都用 1970-01-01 00:00:00
            if (preg_match('/^\/cache\//',$file))
            {
                $fileTime = '1970-01-01 00:00:00';
            }
            $files[$file] = $fileTime;
            $sqlS .= ' OR (fileName = '.$db->quote($file).' AND fileTime = '.$db->quote($fileTime).' )';
            //暫存取代陣列
            $fReplace[$orgFile] = $file;
        }
        
        //搜尋資料庫
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__zoearth_img_flow_files')
            ->where('fileName = ".jpg" '.$sqlS);
        $db->setQuery($query,0,999);
        $rows = $db->loadObjectList();
        //有則取代，無則寫入
        $reS = array();
        if ($rows && is_array($rows) && count($rows) > 0 )
        {
            foreach ($rows as $row)
            {
                unset($files[$row->fileName]);
                if ($paramSets['uploadType'] != 'G' )
                {
                    if ($row->fileUrl != '')
                    {
                        $reS[$row->fileName]['fileName']   = $row->fileUrl;
                        $reS[$row->fileName]['fileServer'] = $row->fileServer;
                    }
                }
                else
                {
                    if ($row->driveUrl != '')
                    {
                        $reS[$row->fileName]['fileName']   = $row->fileUrl;
                        $reS[$row->fileName]['driveUrl']   = $row->driveUrl;
                    }
                }
            }
        }
        
        //取代檔案
        if ($fReplace && is_array($fReplace) && count($fReplace) > 0 )
        {
            $setBody = TRUE;
            foreach ($fReplace as $fKey=>$fFile)
            {
                if (!isset($reS[$fFile]))
                {
                    continue;
                }
                
                if ($paramSets['uploadType'] != 'G' )
                {
                    /* *******20160812 zoearth 這邊是重點*******
                     * 流量不能只用其中一種，應當按照流量配額比例
                     * 目前acsite 流量為 50G  000web 為 100G
                     * 所以分配應當為 100/(50+100) 機率去取代
                     * 但是由於目前都沒有資料所以應當先都用取代方式去製作
                     * ************************************* */
                    //跳過伺服器失效的檔案
                    if (isset($ftp_web_urls[$reS[$fFile]['fileServer']]))
                    {
                        $html = str_replace('"'.$fKey.'"','"'.$ftp_web_urls[$reS[$fFile]['fileServer']].$reS[$fFile]['fileName'].'"',$html);
                    }
                }
                else if ($paramSets['uploadType'] == 'G' )
                {
                    $gimg = 'https://docs.google.com/uc?id='.$reS[$fFile]['driveUrl'];
                    $html = str_replace('"'.$fKey.'"','"'.$gimg.'"',$html);
                }
            }
        }
        
        //寫入剩下的
        if (is_array($files) && count($files) > 0 )
        {
            foreach ($files as $fileName=>$fileTime)
            {
                $db->transactionStart();
                try
                {
                    $data = array(
                            'fileName'   => $fileName,//檔案名稱
                            'fileTime'   => $fileTime,//檔案時間
                            'fileServer' => rand(0,9),//伺服器隨機0-9編號
                    );
                    $data = (object)$data;
                    $db->insertObject('#__zoearth_img_flow_files',$data);
                    $db->transactionCommit();
                }
                catch (Exception $e)
                {
                    $db->transactionRollback();
                    return FALSE;
                }
            }
        }
        $html = str_replace('content____http','content="http',$html);
        return $html;
    }
    
    //關閉FTP
    public static function ftp_close()
    {
        return ftp_close(self::$conn);
    }
}